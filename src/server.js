require('dotenv').config();
require('./databases');
const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const path = require('path');
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/v1', routes);

const port = process.env.PORT || 4000;

app.listen(port, () => {
    console.log(`Api ponto de coleta CenterCell sendo executada na porta ${port}`);
});
