const { Model, DataTypes } = require("sequelize");
class CentrosReparo extends Model {
    static init(connection) {
        super.init({
            Nome: DataTypes.STRING,
            Rua: DataTypes.STRING,
            Numero: DataTypes.STRING,
            CEP: DataTypes.STRING,
            Bairro: DataTypes.STRING,
            Cidade: DataTypes.STRING,
            Estado: DataTypes.STRING,
            Complemento: DataTypes.STRING,
            Telefone1: DataTypes.STRING,
            Telefone2: DataTypes.STRING,
            DtCadastro: DataTypes.DATE,
            numCR: DataTypes.STRING,
			Ativo: DataTypes.INTEGER
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: false,
            updatedAt: false,
            timestamps: false
        });
    }
}

module.exports = CentrosReparo;
