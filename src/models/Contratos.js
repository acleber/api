const { Model, DataTypes } = require('sequelize');

class Contratos extends Model {
	static init(connection) {
		super.init({
			IdContrato: {
				type: DataTypes.INTEGER,
				field: 'IdContrato',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			NumeroContrato: DataTypes.STRING,
			IdClienteContratual: DataTypes.INTEGER,
			DtCadastro: DataTypes.DATE,
			Ativo: DataTypes.INTEGER,
			DtInativa: DataTypes.DATE,
			IdUsuarioGestaoInativa: DataTypes.INTEGER,
			idUsuarioGestaoCadastro: DataTypes.INTEGER,
			idPostoColeta: DataTypes.INTEGER
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = Contratos;
