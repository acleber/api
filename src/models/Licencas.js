const { Model, DataTypes } = require('sequelize');

class Licencas extends Model {
    static init(connection) {
        super.init({
			IdChave: {
				type: DataTypes.INTEGER,
				field: 'IdChave',
				allowNull: false,
				primaryKey: true
			},
			DtAtivacao: DataTypes.DATE,
			FinalValidade: DataTypes.DATE,
			IdUsuarioGestaoCadastro: DataTypes.INTEGER,
			IdPostoColeta: DataTypes.INTEGER,
			idContrato: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER,
			idUsuarioGestaoInativa: DataTypes.INTEGER
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: false,
            updatedAt: false,
            timestamps: false
        });
    }
}

module.exports = Licencas;
