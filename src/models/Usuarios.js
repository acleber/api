const { Model, DataTypes } = require('sequelize');

class Usuarios extends Model {
    static init(connection) {
        super.init({
			Login: DataTypes.STRING,
			Senha: DataTypes.STRING,
			Nome: DataTypes.STRING,
			DtCadastro: DataTypes.DATE,
			Ativo: DataTypes.INTEGER,
			IdUsuarioInativa: DataTypes.INTEGER,
			Email: DataTypes.STRING,
			DtInativa: DataTypes.DATE,
			Token: DataTypes.STRING 
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: false,
            updatedAt: false,
            timestamps: false
        });
    }
}

module.exports = Usuarios;
