const { Model, DataTypes } = require('sequelize');

class ChavesLicenca extends Model {
    static init(connection) {
        super.init({
            Chave: DataTypes.STRING,
            ValidadeDias: DataTypes.INTEGER,
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: true,
            updatedAt: true,
            timestamps: true
        });
    }
}

module.exports = ChavesLicenca;
