const { Model, DataTypes } = require('sequelize');

class Pagamentos extends Model {
	static init(connection) {
		super.init({
			IdPagamento: {
				type: DataTypes.INTEGER,
				field: 'IdPagamento',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			DtEmissao: DataTypes.DATE,
			Valor: DataTypes.FLOAT,
			DtPrevisaoPagamento: DataTypes.DATE,
			DtPagamento: DataTypes.DATE,
			DtCadastro: DataTypes.DATE,
			IdUsuarioCadastro: DataTypes.INTEGER,
			DtInativa: DataTypes.DATE,
			IdUsuarioInativa: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = Pagamentos;
