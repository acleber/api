const { Model, DataTypes } = require('sequelize');

class vincPreOSPercentualConciliacao extends Model {
	static init(connection) {
		super.init({
			IdVincPreOsPercentual: {
				type: DataTypes.INTEGER,
				field: 'IdVincPreOsPercentual',
				allowNull: false,
				primaryKey: true
			},
			PreOS:  DataTypes.INTEGER,
			idPercentualConciliacao:  DataTypes.INTEGER,
			DtCadastro:  DataTypes.DATE,
			IdUsuarioCadastro:  DataTypes.INTEGER,
			IdUsuarioInativa:  DataTypes.INTEGER,
			DtInativa:  DataTypes.DATE,
			Ativo:  DataTypes.INTEGER,
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = vincPreOSPercentualConciliacao;
