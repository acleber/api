const { Model, DataTypes } = require('sequelize');
class VincContratosPagamentos extends Model {
	static init(connection) {
		super.init({
			idContrato: {
				type: DataTypes.INTEGER,
				field: 'idContrato',
				allowNull: false,
				primaryKey: true
			},
			idPagamento:  DataTypes.INTEGER
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = VincContratosPagamentos;
