const { Model, DataTypes } = require('sequelize');

class UsuariosGestao extends Model {
    static init(connection) {
        super.init({
            name: DataTypes.STRING,
            email: DataTypes.STRING,
            password: DataTypes.STRING,
            permission: DataTypes.INTEGER,
            idCentroReparo: DataTypes.INTEGER,
            status: DataTypes.INTEGER,
            passwordResetToken: DataTypes.STRING,
            passwordResetExpires: DataTypes.DATE
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: true,
            updatedAt: true,
            timestamps: true
        });
    }
}

module.exports = UsuariosGestao;
