const { Model, DataTypes } = require('sequelize');

class PostoColeta extends Model {
    static init(connection) {
        super.init({
			idPostoColeta: {
				type: DataTypes.INTEGER,
				field: 'idPostoColeta',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			Nome: DataTypes.STRING,
			DtCadastro: DataTypes.DATE,
			Endereco: DataTypes.STRING,
			Numero: DataTypes.STRING,
			CEP: DataTypes.STRING,
			Bairro: DataTypes.STRING,
			Cidade: DataTypes.STRING,
			Estado: DataTypes.STRING,
			Complemento: DataTypes.STRING,
			Telefone: DataTypes.STRING,
			IdCentroReparo: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: false,
            updatedAt: false,
            timestamps: false
        });
    }
}

module.exports = PostoColeta;
