const { Model, DataTypes } = require('sequelize');

class PercentuaisConciliacao extends Model {
	static init(connection) {
		super.init({
			IdPercentual:{
				type: DataTypes.INTEGER,
				field: 'IdPercentual',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			IdUsuarioGestaoCadastro: DataTypes.INTEGER,
			IdUsuarioGestaoInativa: DataTypes.INTEGER,
			Valor: DataTypes.FLOAT,
			Percentual: DataTypes.INTEGER,
			IdCentroReparo: DataTypes.INTEGER,
			Garantia: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER,
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = PercentuaisConciliacao;
