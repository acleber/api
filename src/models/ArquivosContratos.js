const { Model, DataTypes } = require('sequelize');

class ArquivosContratos extends Model {
	static init(connection) {
		super.init({
			IdArquivoContrato: {
				type: DataTypes.INTEGER,
				field: 'IdArquivoContrato',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			IdContrato: DataTypes.INTEGER,
			DtCadastro: DataTypes.DATE,
			IdUsuarioGestaoCadastro: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER,
			IdUsuarioGestaoInativa: DataTypes.INTEGER,
			DtInativa: DataTypes.DATE,
			CaminhoArquivo: DataTypes.STRING
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = ArquivosContratos;
