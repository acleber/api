const { Model, DataTypes } = require('sequelize');

class ClienteContratual extends Model {
	static init(connection) {
		super.init({
			IdClienteContratual: {
				type: DataTypes.INTEGER,
				field: 'IdClienteContratual',
				allowNull: false,
				primaryKey: true,
				autoIncrement: true
			},
			NomeFantasia: DataTypes.STRING,
			RazaoSocial: DataTypes.STRING,
			Rua: DataTypes.STRING,
			Bairro: DataTypes.STRING,
			CEP: DataTypes.STRING,
			Numero: DataTypes.STRING,
			Estado: DataTypes.STRING,
			CNPJ: DataTypes.STRING,
			InscricaoEstatual: DataTypes.STRING,
			InscricaoMunicipal: DataTypes.STRING,
			Email: DataTypes.STRING,
			Telefone1: DataTypes.STRING,
			Telefone2: DataTypes.STRING,
			NomeContato: DataTypes.STRING,
			DtCadastro: DataTypes.DATE,
			DtInativa: DataTypes.DATE,
			IdUsuarioGestaoInativa: DataTypes.INTEGER,
			IdUsuarioGestaoCadastro: DataTypes.INTEGER,
			Complemento: DataTypes.STRING,
			Cidade: DataTypes.STRING,
			idPostoColeta: DataTypes.INTEGER,
			Ativo: DataTypes.INTEGER,
		}, {
			sequelize: connection,
			freezeTableName: true,
			createdAt: false,
			updatedAt: false,
			timestamps: false
		});
	}
}

module.exports = ClienteContratual;
