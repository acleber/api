const { Model, DataTypes } = require('sequelize');

class PermissoesGestao extends Model {
    static init(connection) {
        super.init({
            permission: DataTypes.STRING,
            description: DataTypes.STRING,
            status: DataTypes.INTEGER
        }, {
            sequelize: connection,
            freezeTableName: true,
            createdAt: true,
            updatedAt: true,
            timestamps: true
        });
    }
}

module.exports = PermissoesGestao;
