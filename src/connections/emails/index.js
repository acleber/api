const nodemailer = require('nodemailer');
const hbs = require('nodemailer-express-handlebars');
const { emailSendingConfiguration } = require('../../configs/mails');
const path = require('path');

const transport = nodemailer.createTransport({
    host: emailSendingConfiguration.host,
    port: emailSendingConfiguration.port,
    secure: emailSendingConfiguration.secure,
    auth: {
        user: emailSendingConfiguration.auth.user,
        pass: emailSendingConfiguration.auth.pass
    },
    tls: {
        rejectUnauthorized: emailSendingConfiguration.rejectUnauthorized
    }
});

const viewPath = path.resolve(__dirname, '..', '..', 'views');

transport.use('compile', hbs({
    viewEngine: {
        partialsDir: path.resolve(viewPath, 'partials'),
        layoutsDir: path.resolve(viewPath, 'layouts'),
        extname: '.hbs'
    },
    extName: '.hbs',
    viewPath: 'src/views'
}));

module.exports = transport;
