exports.emailSendingConfiguration = {
    host: process.env.NODE_MAILER_HOST,
    port: Number(process.env.NODE_MAILER_PORT),
    secure: Boolean(process.env.NODE_MAILER_SECURE),
    auth: {
        user: process.env.NODE_MAILER_USER,
        pass: process.env.NODE_MAILER_PASS,
    },
    rejectUnauthorized: Boolean(!process.env.NODE_MAILER_REJECTUNAUTHORIZED)
};

exports.emailFrom = {
    from: process.env.EMAIL_FROM
};

exports.entryPointUrlAppCenterCell = {
    appCenterCell: process.env.ENTRYPOINT_URL_APP_CENTERCELL
}
