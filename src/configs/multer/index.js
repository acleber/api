const multer = require('multer');
const path = require('path');
const crypto = require('crypto');
const multerS3 = require('multer-s3');
const aws = require('aws-sdk');

const storageTypes = {
    local: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, path.resolve(__dirname, '..', '..', 'archive', 'uploadContract'))
        },
        filename: (req, file, cb) => {
            crypto.randomBytes(16, (err, hash) => {
                if (err) cb(err);

				file.key = `${hash.toString('hex')}-${file.originalname}`;
                file.urlLocal = `${process.env.ENTRYPOINT_URL_API_CENTERCELL_ARCHIVE}/files/${file.key}`

                cb(null, file.key, file.urlLocal);
            });
        }
    }),
    s3: multerS3({
        s3: new aws.S3(),
        bucket: process.env.BUCKET,
        contentType: multerS3.AUTO_CONTENT_TYPE,
        acl: process.env.ACL,
        key: (req, file, cb) => {
            crypto.randomBytes(16, (err, hash) => {
                if (err) cb(err);

                file.key = `${hash.toString('hex')}-${file.originalname}`;

                cb(null, file.key);
            });
        }
    })
}

module.exports = {
    dest: path.resolve(__dirname, '..', '..', 'archive', 'uploadContract'),
    storage: storageTypes[process.env.STORAGE_TYPES],
    limits: {
        fileSize: 10 * 1024 * 1024
    },
    fileFilter: (res, file, cb) => {
        const filetypes = /pdf/;
        const mimetype = filetypes.test(file.mimetype);
        const extname = filetypes.test(path.extname(file.originalname));

        if (mimetype && extname) {
            cb(null, true);
        } else {
            cb(new Error('Invalid file type.'));
        }
    }
}
