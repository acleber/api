require('dotenv').config();
module.exports = {
    dialect: process.env.SQLSERVER_DIALECT,
    host: process.env.SQLSERVER_HOST,
    port: process.env.SQLSERVER_PORT,
    database: process.env.SQLSERVER_DATABASE,
    username: process.env.SQLSERVER_USERNAME,
    password: process.env.SQLSERVER_PASSWORD
}
