'use strict';

module.exports = {
    up: async(queryInterface, Sequelize) => {
        await queryInterface.createTable('UsuariosGestao', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                allowNull: false,
                type: Sequelize.STRING
            },
            email: {
                allowNull: false,
                type: Sequelize.STRING
            },
            password: {
                allowNull: false,
                type: Sequelize.STRING
            },
            permission: {
                allowNull: false,
                type: Sequelize.INTEGER
            },
            idCentroReparo: {
                allowNull: true,
                type: Sequelize.INTEGER
            },
            passwordResetToken: {
                allowNull: true,
                type: Sequelize.STRING
            },
            passwordResetExpires: {
                allowNull: true,
                type: Sequelize.DATE
            },
            status: {
                allowNull: false,
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            }
        });
    },

    down: async(queryInterface, Sequelize) => {
        await queryInterface.dropTable('UsuariosGestao');
    }
};
