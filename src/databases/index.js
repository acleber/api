const Sequelize = require('sequelize');
const dbSqlServer = require('../configs/databases');
const User = require('../models/UsuariosGestao');
const Permission = require('../models/PermissoesGestao');
const CentrosReparo = require('../models/CentrosReparo');
const Usuarios = require('../models/Usuarios');
const PostoColeta = require('../models/PostoColeta');
const ClienteContratual = require('../models/ClienteContratual');
const Contratos = require('../models/Contratos');
const ArquivosContratos = require('../models/ArquivosContratos');
const ChavesLicenca = require('../models/ChavesLicenca');
const PercentuaisConciliacao = require('../models/PercentuaisConciliacao');
const Pagamentos = require('../models/Payments');
const VincContratosPagamentos = require('../models/VincContratosPagamentos');
const Licencas = require('../models/Licencas');
const vincPreOSPercentualConciliacao = require('../models/vincPreOSPercentualConciliacao');

Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
	date = this._applyTimezone(date, options);
	return date.format('YYYY-MM-DD HH:mm:ss.SSS');
};

const connection = new Sequelize(
	dbSqlServer.database,
	dbSqlServer.username,
	dbSqlServer.password, {
	dialect: dbSqlServer.dialect,
	host: dbSqlServer.host,
	port: dbSqlServer.port,
}
);

User.init(connection);
Permission.init(connection);
CentrosReparo.init(connection);
Usuarios.init(connection);
PostoColeta.init(connection);
ClienteContratual.init(connection);
Contratos.init(connection);
ArquivosContratos.init(connection);
ChavesLicenca.init(connection);
PercentuaisConciliacao.init(connection);
Pagamentos.init(connection);
VincContratosPagamentos.init(connection);
Licencas.init(connection);
vincPreOSPercentualConciliacao.init(connection);

module.exports = connection;
