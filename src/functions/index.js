const CryptoJS = require('crypto-js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { privateKeyJWT } = require('../configs/keys');

exports.generateHashPassword = (password) => CryptoJS.MD5(password).toString(CryptoJS.enc.Hex);

exports.generateTokenRandomBytesResetPassword = () => CryptoJS.lib.WordArray.random(50).toString(CryptoJS.enc.Hex);

exports.generateHashPasswordUsuariosPegasus = async (password) => {
	const saltRounds = 10;
	const salt = bcrypt.genSaltSync(saltRounds);
	const hash = bcrypt.hashSync(password, salt);
	
	return hash;
}

exports.genereteTokenForAuthenticate = (userDataForJWT) => {
    const token = jwt.sign(userDataForJWT, privateKeyJWT.key, {
        expiresIn: '10h'
    });

    return token;
}

exports.verifiedToken = (token) => {
    const result = jwt.verify(token, privateKeyJWT.key, (err, decoded) => {
        if (err) {
            console.log(err);
        }
        return decoded
    });
    return result;
}

exports.formatDate = (passwordResetExpires) => {
    const date = new Date(passwordResetExpires).toLocaleDateString('pt-BR');
    const hour = new Date(passwordResetExpires).toLocaleTimeString('pt-BR');

    return {
        date,
        hour
    }
}

exports.formatCurrency = (current) => {
	return current.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' });
};
