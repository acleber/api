const { verifiedToken } = require("../../functions");

exports.authorize = (req, res, next) => {
    try {
        const tokenHeader = req.headers.authorization;

		if (tokenHeader) {
            const parts = tokenHeader.split(' ');
            const [_, token] = parts;
            const authenticate = verifiedToken(token);
            if (authenticate) {
                next();
            } else {
                return res.json({
                    status: 401,
                    message: 'Usuário não possui permissão para acessar o sistema.',
                });
            }
        } else {
            return res.json({
                status: 401,
                message: 'Usuário não possui permissão para acessar o sistema.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.verificationToken = (req, res) => {
    try {
        const tokenVerified = req.params.token;

        if (tokenVerified) {
            const authenticate = verifiedToken(tokenVerified);

            if (authenticate) {
                return res.json({
                    status: 200,
                    message: 'Usuário(a) autenticado(a) com sucesso.',
                });
            } else {
                return res.json({
                    status: 400,
                    message: 'Usuário não possui permissão para acessar o sistema.',
                });
            }
        } else {
            return res.json({
                status: 400,
                message: 'Usuário não possui permissão para acessar o sistema.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}
