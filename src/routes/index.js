const express = require('express');
const multer = require('multer');

const { collectionPointStore, collectionsPointsIndex, collectionsPointsShow } = require('../controllers/colletionPoint');
const { userDelete, userIndex, userShow, userStore, userUpdate } = require('../controllers/user');
const { authenticate, forgotPassword, resetPassword } = require('../controllers/auth');
const { authorize, verificationToken } = require('../middlewares/authorize');
const { permissionIndex, permissionShow, permissionStore, permissionUpdate, permissionDelete } = require('../controllers/permission');
const { centroReparoIndex, centroReparoCreate, centroReparoShow } = require('../controllers/centroReparo');
const { usuariosStore, usuariosIndex, usuariosShow } = require('../controllers/usuarios');
const { clienteContratualUpdate, clienteContratualIndex, clienteContratualShow, clienteContratualShowArchive } = require('../controllers/clienteContratual');
const { contratosIndex, contratosShow } = require('../controllers/contratos');
const { chavesLicencasStore, chavesLicencasShow, chavesLicencasIndex } = require('../controllers/chavesLicencas');
const { percentuaisConciliacaoStore, percentuaisConciliacaoShow } = require('../controllers/percentuaisConciliacao');
const { paymentsIndexDashboard, paymentsStore, paymentsFindCrPcCc, paymentsShow, confirmPayment, deletePayment, updatePayment } = require('../controllers/payments');
const { arquivosContratosStore } = require('../controllers/arquivosContratos');
const { assignLicensesStore, assignLicensesIndex } = require('../controllers/licencas');
const configMulter = require('../configs/multer');
const { reportsConciliacaoIndex, reportsConciliacaoMonth, reportsExportExcel, reportsExportExcelMonth, reportsExportPdf, reportsExportPdfMonth } = require('../controllers/reports');

const routes = express.Router();

routes.get('/auth/authenticated/:token', authorize, verificationToken);
routes.post('/auth/authenticate', authenticate);
routes.patch('/auth/forgotPassword', forgotPassword);
routes.patch('/auth/resetPassword', resetPassword);

routes.get('/user', authorize, userIndex);
routes.get('/user/:id', authorize, userShow);
routes.post('/user/store', authorize, userStore);
routes.put('/user/:id', authorize, userUpdate);
routes.patch('/user/:id', authorize, userDelete);

routes.get('/permission', authorize, permissionIndex);
routes.get('/permission/:id', authorize, permissionShow);
routes.post('/permission/store', authorize, permissionStore);
routes.put('/permission/:id', authorize, permissionUpdate);
routes.patch('/permission/:id', authorize, permissionDelete);

routes.get('/centroReparo', authorize, centroReparoIndex);
routes.get('/centroReparo/:id', authorize, centroReparoShow);
routes.post('/centroReparo/store', authorize, centroReparoCreate);

routes.get('/collectionsPoints/index', authorize, collectionsPointsIndex);
routes.get('/collectionsPoints/:id', authorize, collectionsPointsShow);
routes.post('/collectionsPoints/store',authorize, collectionPointStore);

routes.get('/usuarios/index', authorize, usuariosIndex);
routes.get('/usuarios/:id', authorize, usuariosShow);
routes.post('/usuarios/store', authorize, usuariosStore);

routes.get('/clienteContratual/index', authorize, clienteContratualIndex);
routes.get('/clienteContratualArchive/:idCollectionPoint', authorize, clienteContratualShowArchive);
routes.get('/clienteContratual/:idCollectionPoint', authorize, clienteContratualShow);
routes.put('/clienteContratual/:idClientContract', authorize, clienteContratualUpdate);

routes.get('/licencas/index', authorize, chavesLicencasIndex);
routes.get('/licencas/:chave', authorize, chavesLicencasShow);
routes.post('/licencas/store', authorize, chavesLicencasStore);

routes.get('/contratos/index', authorize, contratosIndex);
routes.get('/contratos/:idContrato', authorize, contratosShow);

routes.post('/arquivosContratos/store/:idContract/:user', multer(configMulter).single('file'), arquivosContratosStore);

routes.get('/percentual/:idCentroReparo/:idPostoColeta?', authorize, percentuaisConciliacaoShow);
routes.post('/percentual/store', authorize, percentuaisConciliacaoStore);

routes.get('/payments/index', authorize, paymentsIndexDashboard);
routes.get('/payments/:idContrato', authorize, paymentsShow);
routes.get('/payments/:idRepairCenter/:idCollectionPoint', authorize, paymentsFindCrPcCc);
routes.post('/payments/store', authorize, paymentsStore);
routes.post('/payments/confirmPayment', authorize, confirmPayment);
routes.post('/payments/deletePayment', authorize, deletePayment);
routes.put('/payments/:id', authorize, updatePayment);

routes.get('/assignLicenses/index', authorize, assignLicensesIndex);
routes.post('/assignLicenses/store', authorize, assignLicensesStore);

routes.get('/reportsConciliacao', authorize, reportsConciliacaoIndex);
routes.get('/reportsConciliacao/:month', authorize, reportsConciliacaoMonth);
routes.get('/reportsConciliacaoExcell', reportsExportExcel);
routes.get('/reportsConciliacaoExcell/:month', reportsExportExcelMonth);
routes.get('/reportsConciliacaoPdf', reportsExportPdf);
routes.get('/reportsConciliacaoPdf/:month', reportsExportPdfMonth);

module.exports = routes;

