const ArquivosContratos = require('../../models/ArquivosContratos');

exports.arquivosContratosStore = async (req, res) => {
	try {
		const { location: url = '' } = req.file;
		const idContract = req.params.idContract;
		const idUsuarioCadastro = req.params.user;
		
		const archiveCreated = await ArquivosContratos.create({
			IdContrato: Number(idContract),
			IdUsuarioGestaoCadastro: Number(idUsuarioCadastro),
			Ativo: 1,
			CaminhoArquivo: url
		});
		
		if (archiveCreated) {		
			return res.json({
				status: 200,
				message: 'Arquivo cadastrado com sucesso.',
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadatrar arquivo.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		});
	}
}
