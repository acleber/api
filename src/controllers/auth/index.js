const User = require("../../models/UsuariosGestao");
const {
    generateHashPassword,
    genereteTokenForAuthenticate,
    generateTokenRandomBytesResetPassword,
    formatDate
} = require("../../functions");
const { emailFrom, entryPointUrlAppCenterCell } = require("../../configs/mails");
const sendEmail = require('../../connections/emails');

exports.authenticate = async(req, res) => {
    try {
        const { email, password } = req.body;

        let user = await User.findOne({ where: { email: email, status: 1 } });

        if (user) {
            const passwordHash = generateHashPassword(password);
            const passwordDatabase = user.password;
            const emailDatabase = user.email;

            if (email === emailDatabase && passwordHash === passwordDatabase) {
                const userDataForJWT = {
                    name: user.name,
                    email: user.email,
                }
                const token = genereteTokenForAuthenticate(userDataForJWT);

                user.password = '';

                return res.json({
                    status: 200,
                    message: 'Usuário(a) logado(a) com sucesso.',
                    user,
                    token
                });
            } else {
                return res.json({
                    status: 400,
                    message: 'Email e/ou senha estão incorreto(s).'
                });
            }
        } else {
            return res.json({
                status: 400,
                message: 'Usuário(a) sem permissão de acesso ao sistema.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.forgotPassword = async(req, res) => {
    try {
        const { email } = req.body;

        const user = await User.findOne({ where: { email: email, status: 1 } });

        if (user === null) {
            return res.json({
                status: 400,
                message: 'Usuário(a) não cadastrado.'
            });
        }

        const passwordResetToken = generateTokenRandomBytesResetPassword();
        const currentTime = new Date();

        currentTime.setHours(currentTime.getHours() + 3);

        const [userUpadated] = await User.update({
            passwordResetToken: passwordResetToken,
            passwordResetExpires: currentTime
        }, {
            where: {
                id: user.id
            }
        });

        if (userUpadated === 1) {
            const { date, hour } = formatDate(currentTime);

            const emailSent = await sendEmail.sendMail({
                from: emailFrom.from,
                to: email,
                subject: 'Alterar senha.',
                template: 'forgotPassword',
                context: {
                    token: passwordResetToken,
                    date,
                    hour,
                    url: entryPointUrlAppCenterCell.appCenterCell,
                    user: user.name,
                }
            });

            if (emailSent) {
                return res.json({
                    status: 200,
                    message: 'E-mail enviado com sucesso.'
                });
            } else {
                return res.json({
                    status: 400,
                    message: 'Erro ao enviar e-mail.'
                });
            }
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao enviar e-mail.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.resetPassword = async(req, res) => {
    try {
        const { password, token } = req.body;

        const user = await User.findOne({ where: { passwordResetToken: token, status: 1 } });

        if (user) {
            const currentTime = new Date();

            if (token !== user.passwordResetToken || currentTime > user.passwordResetExpires) {
                return res.json({
                    status: 400,
                    message: 'Token incorreto e/ou tempo expirado.',
                });
            }

            const [userUpdated] = await User.update({
                password: generateHashPassword(password),
                passwordResetToken: null,
                passwordResetExpires: null
            }, {
                where: {
                    id: user.id
                }
            });

            if (userUpdated === 1) {
                return res.json({
                    status: 200,
                    message: 'Senha alterada com sucesso. Favor fechar esta aba.',
                });
            } else {
                return res.json({
                    status: 400,
                    message: 'Erro ao alterar senha.',
                });
            }
        } else {
            return res.json({
                status: 400,
                message: 'Usuário(a) não cadastrado em nosso sistema.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}
