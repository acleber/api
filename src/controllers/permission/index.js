const Permission = require('../../models/PermissoesGestao');

exports.permissionIndex = async(req, res) => {
    try {
        const permissions = await Permission.findAll({
            where: {
                status: 1
            }
        });

        if (permissions) {
            return res.json({
                status: 200,
                message: 'Permissões selecionados com sucesso.',
                permissions
            });
        } else {
            return res.json({
                status: 200,
                message: 'Não existe permissão cadastrado em nosso banco de dados.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.permissionShow = async(req, res) => {
    try {
        const id = req.params.id;

        const permission = await Permission.findOne({
            where: {
                id,
                status: 1
            }
        });

        if (permission) {
            return res.json({
                status: 200,
                message: 'Permissão encontrado com sucesso.',
                permission
            });
        } else {
            return res.json({
                status: 400,
                message: 'Não existe permissão cadastrado em nosso banco de dados.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.permissionStore = async(req, res) => {
    try {
        const { permission, description } = req.body;

        const [perm, created] = await Permission.findOrCreate({
            where: {
                permission: permission.toUpperCase()
            },
            defaults: {
                permission: permission.toUpperCase(),
                description
            }
        });

        if (created) {
            return res.json({
                status: 200,
                message: 'Permissão cadastrado com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao cadastrar permissão.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.permissionUpdate = async(req, res) => {
    try {
        const id = req.params.id;
        const { permission, description } = req.body;

        const [updated] = await Permission.update({
            permission: permission.toUpperCase(),
            description
        }, {
            where: {
                id
            }
        });

        if (updated === 1) {
            return res.json({
                status: 200,
                message: 'permissão alterado com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao alterar permissão.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.permissionDelete = async(req, res) => {
    try {
        const id = req.params.id;

        const [updated] = await Permission.update({
            status: 0
        }, {
            where: {
                id
            }
        });

        if (updated === 1) {
            return res.json({
                status: 200,
                message: 'Permissão deletado com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao deletar permnissão.'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}
