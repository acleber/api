const { QueryTypes } = require('sequelize');
const CentroReparo = require('../../models/CentrosReparo');

exports.centroReparoIndex = async (req, res) => {
	try {
		const [results, metadata] = await CentroReparo.sequelize.query('SELECT * FROM CentrosReparo');

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe centro de reparo cadastrado no banco de dados.',
			});
		}

		return res.json({
			status: 200,
			message: 'Centros de Reparos selecionados com sucesso.',
			results,
			metadata
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.centroReparoShow = async (req, res) => {
	try {
		const id = req.params.id;

		const results = await CentroReparo.sequelize.query('SELECT * FROM CentrosReparo Where IdCentroReparo = :id', {
			replacements: { id },
			type: QueryTypes.SELECT
		});

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe centro de reparo cadastrado no banco de dados.',
			});
		}

		return res.json({
			status: 200,
			message: 'Centro de Reparo selecionado com sucesso.',
			results
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.centroReparoCreate = async (req, res) => {
	try {
		const { Nome, endereco, numero, bairro, cidade, estado, telefone1, numCR } = req.body;
		let { complemento, telefone2, cep } = req.body;
		complemento.length === 0 ? complemento = null : complemento = complemento;
		telefone2.length === 0 ? telefone2 = null : telefone2 = telefone2;
		cep.length === 9 ? cep = cep.replace('-', '') : cep = null;

		const [results] = await CentroReparo.sequelize.query(`SELECT * FROM CentrosReparo WHERE Nome = :Nome OR numCR = :numCR`, {
			replacements: {
				Nome: Nome,
				numCR: numCR
			},
			type: QueryTypes.SELECT
		});

		if (results) {
			return res.json({
				status: 400,
				message: 'Centros de Reparos já cadastrado.'
			});
		}

		const newDate = new Date();

		const [created] = await CentroReparo.sequelize.query(
			'INSERT INTO CentrosReparo (Nome, Rua, Numero, Bairro, CEP, Cidade, Estado, Complemento, Telefone1, Telefone2, DtCadastro, numCR, Ativo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', {
			replacements: [Nome, endereco, numero, bairro, cep, cidade, estado, complemento, telefone1, telefone2, newDate, numCR, 1],
			type: QueryTypes.INSERT
		});

		if (created) {
			const [result] = await CentroReparo.sequelize.query("SELECT IdCentroReparo FROM CentrosReparo WHERE Nome = :Nome AND numCR = :numCR", {
				type: QueryTypes.SELECT,
				replacements: {
					Nome: Nome,
					numCR: numCR
				}
			});

			if (result) {
				return res.json({
					status: 201,
					message: 'Centros de Reparos criado com sucesso.',
					result
				});
			}
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar centro de reparos.'
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}
