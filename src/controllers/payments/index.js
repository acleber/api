const { QueryTypes } = require('sequelize');
const CentrosReparo = require("../../models/CentrosReparo");
const Pagamentos = require("../../models/Payments");
const VincContratosPagamentos = require("../../models/VincContratosPagamentos");

exports.paymentsFindCrPcCc = async (req, res) => {
	try {
		const idRepairCenter = req.params.idRepairCenter;
		const idCollectionPoint = req.params.idCollectionPoint;

		const [result] = await CentrosReparo.sequelize.query(
			`SELECT 
			CentrosReparo.Nome as CRNome, 
			PostoColeta.Nome as PCNome, 
			ClienteContratual.CNPJ, 
			ClienteContratual.NomeContato,
			ClienteContratual.Email,
			Contratos.IdContrato
		FROM 
			CentrosReparo,
			PostoColeta,
			ClienteContratual,
			Contratos
		WHERE
			CentrosReparo.IdCentroReparo = :idRepairCenter and 
			PostoColeta.IdCentroReparo = CentrosReparo.IdCentroReparo and
			ClienteContratual.idPostoColeta = :idCollectionPoint and
			ClienteContratual.idPostoColeta = PostoColeta.idPostoColeta and
			ClienteContratual.IdClienteContratual = Contratos.IdClienteContratual and
			Contratos.idPostoColeta = :idCollectionPoint`,
			{
				replacements: { idCollectionPoint, idRepairCenter },
				type: QueryTypes.SELECT
			}
		);

		if (result) {
			return res.json({
				status: 200,
				message: 'Dados Selecionado com sucesso.',
				result
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar dados.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		});
	}
}

exports.paymentsShow = async (req, res) => {
	try {
		const idContrato = req.params.idContrato;
		let contador = 0;

		let result = await Pagamentos.sequelize.query(
			`SELECT * from 
				Pagamentos as PG, 
				vincContratosPagamentos as VCP
			where
				VCP.idContrato = :idContrato and 
				PG.IdPagamento = VCP.idPagamento and
				PG.Ativo = 1`,
			{
				replacements: { idContrato },
				type: QueryTypes.SELECT
			}
		);

		result.forEach(element => {
			element.parcela = contador + 1;
			contador++;
		});

		if (result) {
			return res.json({
				status: 200,
				message: 'Pagamentos Selecionados com sucesso.',
				result
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar dados.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		});
	}
}

exports.paymentsIndexDashboard = async (req, res) => {
	try {
		const [payments] = await Pagamentos.sequelize.query(
			`SELECT COUNT(DtPrevisaoPagamento) AS totalPagamentoAbertos, COUNT(DtPagamento) AS totalPagamentoRealizados FROM Pagamentos WHERE Ativo = 1`,
			{
				type: QueryTypes.SELECT
			}
		);

		const [deletePayments] = await Pagamentos.sequelize.query(
			`SELECT COUNT(Ativo) AS totalPagamentoCancelados FROM Pagamentos WHERE Ativo = 0`,
			{
				type: QueryTypes.SELECT
			}
		);

		if (payments) {
			return res.json({
				status: 200,
				message: 'Pagamentos Selecionados com sucesso.',
				payments: [
					{
						name: 'Abertos',
						color: '#D39B09',
						value: payments.totalPagamentoAbertos - payments.totalPagamentoRealizados
					},
					{
						name: 'Pagos',
						color: '#02B915',
						value: payments.totalPagamentoRealizados
					},
					{
						name: 'Cancelados',
						color: '#FF3131',
						value: deletePayments.totalPagamentoCancelados
					}
				]
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar dados.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		});
	}
}

exports.paymentsStore = async (req, res) => {
	try {
		const { quantidadesParcelas, valor, idUsuarioCadastro, idContrato } = req.body;

		const issuanceDate = new Date();
		const paymentForecastDate = new Date();

		let cont = 0;

		for (let x = 0; x < quantidadesParcelas; x++) {
			const result = await Pagamentos.create(
				{
					Valor: valor,
					DtEmissao: issuanceDate,
					DtPrevisaoPagamento: paymentForecastDate.setDate(paymentForecastDate.getDate() + 30),
					Ativo: 1
				}
			);
			await VincContratosPagamentos.create(
				{
					idContrato: idContrato,
					idPagamento: result.dataValues.IdPagamento
				}
			);
			cont++
		}

		if (cont === quantidadesParcelas) {
			return res.json({
				status: 201,
				message: 'Pagamentos cadastrados com sucesso.'
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar pagamentos.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.confirmPayment = async (req, res) => {
	try {
		const { idPagamento } = req.body;

		const dataPagamento = new Date();

		const result = await Pagamentos.update(
			{ DtPagamento: dataPagamento },
			{
				where: {
					IdPagamento: idPagamento
				}
			});

		if (result) {
			return res.json({
				status: 204,
				message: 'Pagamento baixado com sucesso.'
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao baixar pagamento.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.deletePayment = async (req, res) => {
	try {
		const { idPagamento } = req.body;

		const result = await Pagamentos.update(
			{ Ativo: 0 },
			{
				where: {
					IdPagamento: idPagamento
				}
			});

		if (result) {
			return res.json({
				status: 204,
				message: 'Pagamento deletado com sucesso.'
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao deletar pagamento.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.updatePayment = async (req, res) => {
	try {
		const { paymentsUpdateValor,
			paymentsUpdateDataEmissao,
			paymentsUpdateDataPrevissaoPagamento,
			paymentsUpdateDataPagamento
		} = req.body;

		const id = req.params.id;

		const result = await Pagamentos.update(
			{ 
				Valor: paymentsUpdateValor,
				DtEmissao: paymentsUpdateDataEmissao,
				DtPrevisaoPagamento: paymentsUpdateDataPrevissaoPagamento,
				DtPagamento: paymentsUpdateDataPagamento ? paymentsUpdateDataPagamento : null
			},
			{
				where: {
					IdPagamento: id
				}
			});

		if (result) {
			return res.json({
				status: 204,
				message: 'Pagamento alterado com sucesso.'
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao alterar pagamento.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}
