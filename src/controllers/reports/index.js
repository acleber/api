const { QueryTypes } = require("sequelize");
const vincPreOSPercentualConciliacao = require("../../models/vincPreOSPercentualConciliacao");
const xl = require('excel4node');
const fs = require('fs');
const pdf = require('html-pdf');
const ejs = require('ejs');
const path = require('path');
const { formatDate, formatCurrency } = require("../../functions");

const queryReportsMont = async (month) => {
	return results = await vincPreOSPercentualConciliacao.sequelize.query(
		`SELECT
			vincPreOSPercentualConciliacao.IdVincPreOsPercentual,
			vincPreOSPercentualConciliacao.PreOS,
			Marcas.Descricao AS marca,
			Modelos.Descricao AS Modelo,
		CASE PreOS.IdTipoAtendimento WHEN 5 then 'Garantia' WHEN 6 THEN 'Fora de Garantia' END AS TipoAtendimento,
			PercentuaisConciliacao.IdPercentual,
			PercentuaisConciliacao.Valor,
			PercentuaisConciliacao.Percentual,
			vincPreOSPercentualConciliacao.DtCadastro
		FROM (((((vincPreOSPercentualConciliacao 
			INNER JOIN PercentuaisConciliacao ON
				PercentuaisConciliacao.IdPercentual = vincPreOSPercentualConciliacao.idPercentualConciliacao)
			INNER JOIN PreOS ON
				PreOS.PreOS = vincPreOSPercentualConciliacao.PreOS)
			INNER JOIN Marcas ON
				Marcas.Descricao = 'Samsung')
			INNER JOIN ProdutoMultimarcas ON
				PreOS.IdProduto = ProdutoMultimarcas.IdProduto)
				INNER JOIN Modelos ON
				ProdutoMultimarcas.IdModelo = Modelos.IdModelo) 
		where MONTH(vincPreOSPercentualConciliacao.DtCadastro) = :month AND YEAR(vincPreOSPercentualConciliacao.DtCadastro) = 2021
		ORDER BY vincPreOSPercentualConciliacao.IdVincPreOsPercentual`,
		{
			replacements: { month },
			type: QueryTypes.SELECT
		}
	);
}

const queryReports = async () => {
	const [results, metadata] = await vincPreOSPercentualConciliacao.sequelize.query(
		`SELECT
			vincPreOSPercentualConciliacao.IdVincPreOsPercentual,
			vincPreOSPercentualConciliacao.PreOS,
			Marcas.Descricao AS marca,
			Modelos.Descricao AS Modelo,
		CASE PreOS.IdTipoAtendimento WHEN 5 then 'Garantia' WHEN 6 THEN 'Fora de Garantia' END AS TipoAtendimento,
			PercentuaisConciliacao.IdPercentual,
			PercentuaisConciliacao.Valor,
			PercentuaisConciliacao.Percentual,
			vincPreOSPercentualConciliacao.DtCadastro
		FROM (((((vincPreOSPercentualConciliacao 
			INNER JOIN PercentuaisConciliacao ON
				PercentuaisConciliacao.IdPercentual = vincPreOSPercentualConciliacao.idPercentualConciliacao)
			INNER JOIN PreOS ON
				PreOS.PreOS = vincPreOSPercentualConciliacao.PreOS)
			INNER JOIN Marcas ON
				Marcas.Descricao = 'Samsung')
			INNER JOIN ProdutoMultimarcas ON
				PreOS.IdProduto = ProdutoMultimarcas.IdProduto)
				INNER JOIN Modelos ON
				ProdutoMultimarcas.IdModelo = Modelos.IdModelo)
		ORDER BY vincPreOSPercentualConciliacao.IdVincPreOsPercentual`,
	);

	return results;
}

const calculaValor = ((valor, percentual) => {
	return (percentual / 100) * valor;
});

const returnMes = (month) => {
	switch (month) {
		case 1:
			return 'Janeiro';
		case 2:
			return 'Fevereiro';
		case 3:
			return 'Março';
		case 4:
			return 'Abril';
		case 5:
			return 'Maio';
		case 6:
			return 'Junho';
		case 7:
			return 'Julho';
		case 8:
			return 'Agosto';
		case 9:
			return 'Setembro';
		case 10:
			return 'Outubro';
		case 11:
			return 'Novembro';
		case 12:
			return 'Dezembro';
		default:
			break;
	}
}

exports.reportsConciliacaoIndex = async (req, res) => {
	try {
		const [results, metadata] = await vincPreOSPercentualConciliacao.sequelize.query(
			`SELECT
				vincPreOSPercentualConciliacao.IdVincPreOsPercentual,
				vincPreOSPercentualConciliacao.PreOS,
				Marcas.Descricao AS marca,
				Modelos.Descricao AS Modelo,
			CASE PreOS.IdTipoAtendimento WHEN 5 then 'Garantia' WHEN 6 THEN 'Fora de Garantia' END AS TipoAtendimento,
				PercentuaisConciliacao.IdPercentual,
				PercentuaisConciliacao.Valor,
				PercentuaisConciliacao.Percentual,
				vincPreOSPercentualConciliacao.DtCadastro
			FROM (((((vincPreOSPercentualConciliacao 
				INNER JOIN PercentuaisConciliacao ON
					PercentuaisConciliacao.IdPercentual = vincPreOSPercentualConciliacao.idPercentualConciliacao)
				INNER JOIN PreOS ON
					PreOS.PreOS = vincPreOSPercentualConciliacao.PreOS)
				INNER JOIN Marcas ON
					Marcas.Descricao = 'Samsung')
				INNER JOIN ProdutoMultimarcas ON
					PreOS.IdProduto = ProdutoMultimarcas.IdProduto)
					INNER JOIN Modelos ON
					ProdutoMultimarcas.IdModelo = Modelos.IdModelo) 
			ORDER BY vincPreOSPercentualConciliacao.IdVincPreOsPercentual`
		);

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe dados cadastrados.',
			});
		}

		if (results) {
			return res.json({
				status: 200,
				message: 'Dados selecionados com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar dados.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.reportsConciliacaoMonth = async (req, res) => {
	try {
		const month = req.params.month;

		const results = await vincPreOSPercentualConciliacao.sequelize.query(
			`SELECT
				vincPreOSPercentualConciliacao.IdVincPreOsPercentual,
				vincPreOSPercentualConciliacao.PreOS,
				Marcas.Descricao AS marca,
				Modelos.Descricao AS Modelo,
			CASE PreOS.IdTipoAtendimento WHEN 5 then 'Garantia' WHEN 6 THEN 'Fora de Garantia' END AS TipoAtendimento,
				PercentuaisConciliacao.IdPercentual,
				PercentuaisConciliacao.Valor,
				PercentuaisConciliacao.Percentual,
				vincPreOSPercentualConciliacao.DtCadastro
			FROM (((((vincPreOSPercentualConciliacao 
				INNER JOIN PercentuaisConciliacao ON
					PercentuaisConciliacao.IdPercentual = vincPreOSPercentualConciliacao.idPercentualConciliacao)
				INNER JOIN PreOS ON
					PreOS.PreOS = vincPreOSPercentualConciliacao.PreOS)
				INNER JOIN Marcas ON
					Marcas.Descricao = 'Samsung')
				INNER JOIN ProdutoMultimarcas ON
					PreOS.IdProduto = ProdutoMultimarcas.IdProduto)
					INNER JOIN Modelos ON
					ProdutoMultimarcas.IdModelo = Modelos.IdModelo) 
			where MONTH(vincPreOSPercentualConciliacao.DtCadastro) = :month AND YEAR(vincPreOSPercentualConciliacao.DtCadastro) = 2021
			ORDER BY vincPreOSPercentualConciliacao.IdVincPreOsPercentual`,
			{
				replacements: { month },
				type: QueryTypes.SELECT
			}
		);

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe dados cadastrados.',
			});
		}

		if (results) {
			return res.json({
				status: 200,
				message: 'Dados selecionados com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar dados.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.reportsExportExcelMonth = async (req, res) => {
	const month = req.params.month;
	const data = await queryReportsMont(month);
	const reportConciliation = new xl.Workbook({
		workbookView: {
			activeTab: 1
		}
	});
	const conciliation = reportConciliation.addWorksheet('Conciliação');

	conciliation.cell(1, 1).string('PréOS');
	conciliation.cell(1, 2).string('Marca');
	conciliation.cell(1, 3).string('Modelo');
	conciliation.cell(1, 4).string('Tipo Atendimento');
	conciliation.cell(1, 5).string('Valor a Pagar');
	conciliation.cell(1, 6).string('Valor a Receber');
	conciliation.cell(1, 7).string('Data');

	let line = 2;
	let valorPagar = 0;
	let valorReceber = 0;

	data.forEach(element => {
		conciliation.cell(line, 1).number(element.PreOS);
		conciliation.cell(line, 2).string(element.marca);
		conciliation.cell(line, 3).string(element.Modelo);
		conciliation.cell(line, 4).string(element.TipoAtendimento);
		conciliation.cell(line, 5).number(element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0);
		conciliation.cell(line, 6).number(element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0);
		conciliation.cell(line, 7).date(element.DtCadastro).style({ numberFormat: 'dd-mm-yyyy' });

		line += 1;
		valorPagar += element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
		valorReceber += element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
	});

	conciliation.cell(line + 1, 1).string('Total a Pagar');
	conciliation.cell(line + 1, 2).number(Number(valorPagar));

	conciliation.cell(line + 1, 4).string('Total a Receber');
	conciliation.cell(line + 1, 5).number(Number(valorReceber));

	conciliation.cell(line + 1, 6).string('Total no Perído');
	conciliation.cell(line + 1, 7).number(Number(valorReceber - valorPagar));

	await reportConciliation.write(`src/archive/excel/ReportConciliation.xlsx`);

	setTimeout(() => {
		fs.readFile(`src/archive/excel/ReportConciliation.xlsx`, function (err, data) {
			if (err) throw err;
			fs.unlink(`src/archive/excel/ReportConciliation.xlsx`, function (err) {
				if (err) throw err;
			});
			res.setHeader('Content-Disposition', `attachment;filename=ReportConciliation.xlsx`);
			return res.send(data);
		});
	}, 5000);
}

exports.reportsExportExcel = async (req, res) => {
	const data = await queryReports();
	const reportConciliation = new xl.Workbook({
		workbookView: {
			activeTab: 1
		}
	});
	const conciliation = reportConciliation.addWorksheet('Conciliação');

	conciliation.cell(1, 1).string('PréOS');
	conciliation.cell(1, 2).string('Marca');
	conciliation.cell(1, 3).string('Modelo');
	conciliation.cell(1, 4).string('Tipo Atendimento');
	conciliation.cell(1, 5).string('Valor a Pagar');
	conciliation.cell(1, 6).string('Valor a Receber');
	conciliation.cell(1, 7).string('Data');

	let line = 2;
	let valorPagar = 0;
	let valorReceber = 0;

	data.forEach(element => {
		conciliation.cell(line, 1).number(element.PreOS);
		conciliation.cell(line, 2).string(element.marca);
		conciliation.cell(line, 3).string(element.Modelo);
		conciliation.cell(line, 4).string(element.TipoAtendimento);
		conciliation.cell(line, 5).number(element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0);
		conciliation.cell(line, 6).number(element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0);
		conciliation.cell(line, 7).date(element.DtCadastro).style({ numberFormat: 'dd-mm-yyyy' });

		line += 1;
		valorPagar += element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
		valorReceber += element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
	});

	conciliation.cell(line + 1, 1).string('Total a Pagar');
	conciliation.cell(line + 1, 2).number(Number(valorPagar));

	conciliation.cell(line + 1, 4).string('Total a Receber');
	conciliation.cell(line + 1, 5).number(Number(valorReceber));

	conciliation.cell(line + 1, 6).string('Total no Período');
	conciliation.cell(line + 1, 7).number(Number(valorReceber - valorPagar));

	await reportConciliation.write(`src/archive/excel/ReportConciliation.xlsx`);

	setTimeout(() => {
		fs.readFile(`src/archive/excel/ReportConciliation.xlsx`, function (err, data) {
			if (err) throw err;
			fs.unlink(`src/archive/excel/ReportConciliation.xlsx`, function (err) {
				if (err) throw err;
			});
			res.setHeader('Content-Disposition', `attachment;filename=ReportConciliation.xlsx`);
			return res.send(data);
		});
	}, 5000);
}

exports.reportsExportPdfMonth = async (req, res) => {
	const month = req.params.month;
	const data = await queryReportsMont(month);
	const filePath = path.join(__dirname, '..', '..', 'views', 'pdf.ejs');
	const newArrayData = [];
	let totalPagar = 0;
	let totalReceber = 0;

	data.forEach(element => {
		newArrayData.push({
			PreOS: element.PreOS,
			Marca: element.marca,
			Modelo: element.Modelo,
			TipoAtendimento: element.TipoAtendimento,
			ValorPagar: element.TipoAtendimento === 'Fora de Garantia' ? formatCurrency(calculaValor(element.Valor, element.Percentual)) : formatCurrency(0),
			ValorReceber: element.TipoAtendimento === 'Garantia' ? formatCurrency(calculaValor(element.Valor, element.Percentual)) : formatCurrency(0),
			Data: formatDate(element.DtCadastro).date
		});
		totalPagar += element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
		totalReceber += element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
	});

	const totaisPagar = formatCurrency(totalPagar);
	const totaisReceber = formatCurrency(totalReceber);
	const totaisGeral = formatCurrency(totalReceber - totalPagar);


	ejs.renderFile(filePath,
		{
			data: newArrayData,
			totaisPagar: totaisPagar,
			totaisReceber: totaisReceber,
			totaisGeral: totaisGeral,
			mes: returnMes(Number(month)),
			maisMenos: (totalReceber - totalPagar) > 0 ? '+' : '-'
		}, (err, html) => {
			if (err) {
				throw err;
			}

			const options = {
				paginationOffset: 1,
				header: {
					height: "10mm",
				},
				footer: {
					height: "15mm",
					contents: {
						default: '<span style="color: #444; textAlign: right;">Pegazus System - {{page}}</span>/<span>{{pages}}</span>',
					}
				}
			}

			pdf.create(html, options).toFile('src/archive/pdf/ReportConciliation.pdf', (err, result) => {
				if (err) {
					throw err;
				}

				setTimeout(() => {
					fs.readFile(`src/archive/pdf/ReportConciliation.pdf`, function (err, data) {
						if (err) throw err;
						fs.unlink(`src/archive/pdf/ReportConciliation.pdf`, function (err) {
							if (err) throw err;
						});
						res.setHeader('Content-Disposition', `attachment;filename=ReportConciliation.pdf`);
						return res.send(data);
					});
				}, 3000);
			});
		});
}

exports.reportsExportPdf = async (req, res) => {
	const data = await queryReports();
	const filePath = path.join(__dirname, '..', '..', 'views', 'pdf.ejs');
	const newArrayData = [];
	let totalPagar = 0;
	let totalReceber = 0;

	data.forEach(element => {
		newArrayData.push({
			PreOS: element.PreOS,
			Marca: element.marca,
			Modelo: element.Modelo,
			TipoAtendimento: element.TipoAtendimento,
			ValorPagar: element.TipoAtendimento === 'Fora de Garantia' ? formatCurrency(calculaValor(element.Valor, element.Percentual)) : formatCurrency(0),
			ValorReceber: element.TipoAtendimento === 'Garantia' ? formatCurrency(calculaValor(element.Valor, element.Percentual)) : formatCurrency(0),
			Data: formatDate(element.DtCadastro).date
		});
		totalPagar += element.TipoAtendimento === 'Fora de Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
		totalReceber += element.TipoAtendimento === 'Garantia' ? calculaValor(element.Valor, element.Percentual) : 0;
	});

	const totaisPagar = formatCurrency(totalPagar);
	const totaisReceber = formatCurrency(totalReceber);
	const totaisGeral = formatCurrency(totalReceber - totalPagar);

	ejs.renderFile(filePath,
		{
			data: newArrayData,
			totaisPagar: totaisPagar,
			totaisReceber: totaisReceber,
			totaisGeral: totaisGeral,
			mes: 'Janeiro a Dezembro',
			maisMenos: (totalReceber - totalPagar) > 0 ? '+' : '-'
		}, (err, html) => {
			if (err) {
				throw err;
			}

			const options = {
				paginationOffset: 1,
				header: {
					height: "10mm",
				},
				footer: {
					height: "15mm",
					contents: {
						default: '<span style="color: #444; textAlign: right;">Pegazus System - {{page}}</span>/<span>{{pages}}</span>',
					}
				}
			}

			pdf.create(html, options).toFile('src/archive/pdf/ReportConciliation.pdf', (err, result) => {
				if (err) {
					throw err;
				}

				setTimeout(() => {
					fs.readFile(`src/archive/pdf/ReportConciliation.pdf`, function (err, data) {
						if (err) throw err;
						fs.unlink(`src/archive/pdf/ReportConciliation.pdf`, function (err) {
							if (err) throw err;
						});
						res.setHeader('Content-Disposition', `attachment;filename=ReportConciliation.pdf`);
						return res.send(data);
					});
				}, 3000);
			});
		});
}
