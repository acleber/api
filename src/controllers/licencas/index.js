const Contratos = require("../../models/Contratos");
const Licencas = require("../../models/Licencas");
const { QueryTypes } = require("sequelize");

exports.assignLicensesIndex = async (req, res) => {
	try {
		const results = await Licencas.findAll();

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe chaves de licenças cadastradas.',
            });
        }

		if (results) {
			return res.json({
				status: 200,
				message: 'Licenças selecionadas com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar licenças.',
			});
		}
	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}

exports.assignLicensesStore = async (req, res) => {
	try {
		const { idKeyLicenses, idCollectionPoint, daysValidity, sub } = req.body;
		
		const [results] = await Contratos.sequelize.query('SELECT IdContrato FROM Contratos WHERE idPostoColeta = :idCollectionPoint', {
			replacements: { idCollectionPoint },
			type: QueryTypes.SELECT
		});

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe contrato para o posto de coleta.',
			});
		}

		const IdContrato = results.IdContrato;

		const dataValidade = new Date();
		let newDate = new Date();
		newDate.setDate(dataValidade.getDate() + daysValidity);

		const licenses = await Licencas.create({
			IdChave: idKeyLicenses,
			DtAtivacao: dataValidade,
			FinalValidade: newDate,
			IdUsuarioGestaoCadastro: Number(sub),
			IdPostoColeta: idCollectionPoint,
			idContrato: IdContrato,
			Ativo: 1 
		});

		if (licenses) {		
			return res.json({
				status: 201,
				message: 'Licença cadastrada com sucesso.',
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadatrar licença.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}
