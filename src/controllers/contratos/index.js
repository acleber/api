const Contratos = require("../../models/Contratos");
const { QueryTypes } = require("sequelize");

exports.contratosIndex = async (req, res) => {
	try {

		const [results] = await Contratos.sequelize.query('SELECT * FROM Contratos ');

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe contratos cadastrados.',
            });
        }

		if (results) {
			return res.json({
				status: 200,
				message: 'Contratos selecionados com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar chaves de licenças.',
			});
		}

	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}

exports.contratosShow = async (req, res) => {
	try {
		const idContrato = req.params.idContrato;

		const results = await Contratos.sequelize.query('SELECT * FROM Contratos WHERE IdContrato = :idContrato', {
			replacements: { idContrato },
			type: QueryTypes.SELECT
		});

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe contrato cadastrado.',
            });
        }

		if (results) {
			return res.json({
				status: 201,
				message: 'Contrato selecionado com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar o contrato.',
			});
		}

	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}
