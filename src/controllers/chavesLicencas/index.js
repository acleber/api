const ChavesLicenca = require("../../models/ChavesLicenca");
const { QueryTypes } = require("sequelize");

exports.chavesLicencasIndex = async (req, res) => {
	try {

		const [results] = await ChavesLicenca.sequelize.query('select * from ChavesLicenca where IdChave NOT IN (select idChave from Licencas)');
		const [totalLicenca] = await ChavesLicenca.sequelize.query('SELECT COUNT(IdChave) as totalLicenca FROM ChavesLicenca');

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe chaves de licenças cadastradas.',
            });
        }

		if (results) {
			return res.json({
				status: 200,
				message: 'Chaves de licenças selecionadas com sucesso.',
				results,
				totalLicencaStore: totalLicenca[0].totalLicenca
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar chaves de licenças.',
			});
		}

	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}

exports.chavesLicencasShow = async (req, res) => {
	try {
		const chave = req.params.chave;

		const [results] = await ChavesLicenca.sequelize.query('SELECT * FROM ChavesLicenca WHERE Chave = :chave', {
			replacements: { chave },
			type: QueryTypes.SELECT
		});

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe chaves de licenças cadastradas.',
            });
        }

		if (results) {
			return res.json({
				status: 201,
				message: 'Chave de licença selecionada com sucesso.',
				results
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar chave de licença.',
			});
		}

	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}

exports.chavesLicencasStore = async (req, res) => {
	try {
		const { chave, validadeDias } = req.body;

		const results = await ChavesLicenca.sequelize.query('SELECT * FROM ChavesLicenca WHERE Chave = :chave', {
			replacements: { chave },
			type: QueryTypes.SELECT
		});

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe chaves de licenças cadastradas.',
            });
        }

		const userCreated = await ChavesLicenca.sequelize.query('INSERT INTO ChavesLicenca (Chave, ValidadeDias) VALUES (?,?)', {
			replacements: [chave, validadeDias],
			type: QueryTypes.INSERT
		});

		if (userCreated) {
			return res.json({
				status: 201,
				message: 'Chave de licença cadastrada com sucesso.',
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar chave de licença.',
			});
		}

	} catch (error) {
		return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
	}
}
