const PercentuaisConciliacao = require("../../models/PercentuaisConciliacao");

exports.percentuaisConciliacaoShow = async (req, res) => {
	try {
		const idCentroReparo = req.params.idCentroReparo;
		const idPostoColeta = req.params.idPostoColeta;

		let percentualConciliacao = undefined;

		if(idPostoColeta === undefined) {
			percentualConciliacao = await PercentuaisConciliacao.findAll({
				attributes: ['IdPercentual', 'Percentual', 'Valor', 'Garantia'],
				where: {
					IdCentroReparo: idCentroReparo,
					Ativo: 1
				}
			});
		} else {
			percentualConciliacao = await PercentuaisConciliacao.findAll({
				attributes: ['IdPercentual', 'Percentual', 'Valor', 'Garantia'],
				where: {
					IdCentroReparo: idCentroReparo,
					IdPostoColeta: idPostoColeta,
					Ativo: 1
				}
			});
		}
		
		if (percentualConciliacao) {
			return res.json({
				status: 200,
				message: 'Porcentagens selecionadas com sucesso.',
				percentualConciliacao
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao selecionar porcentagens.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}

exports.percentuaisConciliacaoStore = async (req, res) => {
	try {
		const { valorGarantia, percentualGarantia, valorTaxaAnalise, percentualTaxaAnalise, idCentroReparo, idUsuarioCadastro } = req.body;

		const createdGaratia = await PercentuaisConciliacao.create(
			{
				IdUsuarioGestaoCadastro: idUsuarioCadastro,
				Valor: valorGarantia,
				Percentual: percentualGarantia,
				IdCentroReparo: idCentroReparo,
				Garantia: 1,
				Ativo: 1
			}
		);

		const createdTaxaAnalise = await PercentuaisConciliacao.create(
			{
				IdUsuarioGestaoCadastro: idUsuarioCadastro,
				Valor: valorTaxaAnalise,
				Percentual: percentualTaxaAnalise,
				IdCentroReparo: idCentroReparo,
				Garantia: 0,
				Ativo: 1
			}
		);

		if (createdGaratia && createdTaxaAnalise) {
			return res.json({
				status: 201,
				message: 'Porcentagens cadastradas com sucesso.'
			});
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar porcentagem.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}


