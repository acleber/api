const Usuarios = require("../../models/Usuarios");
const { generateHashPasswordUsuariosPegasus } = require("../../functions");
const { QueryTypes } = require("sequelize");

exports.usuariosIndex = async (req, res) => {
	try {
        const [results, metadata] = await Usuarios.sequelize.query('SELECT * FROM Usuarios');

        if (!results) {
            return res.json({
                status: 200,
                message: 'Não existe usuários cadastrado no banco de dados.',
            });
        }

        return res.json({
            status: 200,
            message: 'Usuários selecionados com sucesso.',
            results,
            metadata
        });
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.usuariosShow = async (req, res) => {
	try {
		const id = req.params.id;

        const results = await Usuarios.sequelize.query('SELECT * FROM Usuarios WHERE IdPostoColeta = :id', {
			replacements: { id },
			type: QueryTypes.SELECT
		});

        if (!results) {
            return res.json({
                status: 400,
                message: 'Não existe usuário cadastrado no banco de dados.',
            });
        }

        return res.json({
            status: 200,
            message: 'Usuários selecionados com sucesso.',
            results
        });
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.usuariosStore = async (req, res) => {
	try {
		const { Login, Senha, NomeCompletoUsuario, Email, IdPostoColeta } = req.body;

		const user = await Usuarios.sequelize.query('SELECT * from Usuarios WHERE Email = :Email', {
			replacements: { Email },
			type: QueryTypes.SELECT
		});

		if (user.length > 0) {
			return res.json({
				status: 400,
				message: 'Usuário já cadastrado.',
			});
		}

		const newPassword = await generateHashPasswordUsuariosPegasus(Senha);

		const [userCreated] = await Usuarios.sequelize.query('INSERT INTO Usuarios (Login, Senha, Nome, Ativo, IdPostoColeta, Email) VALUES (?,?,?,?,?,?)', {
			replacements: [Login, newPassword, NomeCompletoUsuario, 1, IdPostoColeta, Email],
			type: QueryTypes.INSERT
		});

		if (userCreated) {
			const [result] = await Usuarios.sequelize.query('SELECT IdUsuario, Nome, Email from Usuarios WHERE Email = :Email', {
				replacements: { Email },
				type: QueryTypes.SELECT
			});

			if (result) {
				return res.json({
					status: 201,
					message: 'Usuário cadastrado com sucesso.',
					result
				});
			} else {
				return res.json({
					status: 400,
					message: 'Erro ao cadastrar usuário.',
				});
			}
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar usuário.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
}
