const { QueryTypes } = require("sequelize");
const ClienteContratual = require("../../models/ClienteContratual");
const CollectionPoint = require("../../models/PostoColeta");

exports.collectionsPointsIndex = async (req, res) => {
	try {
		const [results, metadata] = await CollectionPoint.sequelize.query('SELECT * FROM PostoColeta');

		if (!results) {
			return res.json({
				status: 200,
				message: 'Não existe posto de coleta cadastrado no banco de dados.',
			});
		}

		return res.json({
			status: 200,
			message: 'Posto de coleta selecionados com sucesso.',
			results,
			metadata
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
};

exports.collectionsPointsShow = async (req, res) => {
	try {
		const id = req.params.id;

		const results = await CollectionPoint.sequelize.query('SELECT * FROM PostoColeta WHERE idPostoColeta = :id', {
			replacements: { id },
			type: QueryTypes.SELECT
		});

		if (!results) {
			return res.json({
				status: 400,
				message: 'Não existe posto de coleta cadastrado no banco de dados.',
			});
		}

		return res.json({
			status: 200,
			message: 'Posto de coleta selecionado com sucesso.',
			results
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
};

exports.collectionPointStore = async (req, res, next) => {
	try {
		const { Nome, endereco, numero, bairro, cidade, estado, telefone, IdCentroReparo, idUsuarioCadastro } = req.body;
		let { complemento, cep } = req.body;
		complemento.length === 0 ? complemento = null : complemento = complemento;
		cep.length === 9 ? cep = cep.replace('-', '') : cep = null;

		const postoColeta = await CollectionPoint.sequelize.query('SELECT * from PostoColeta WHERE Nome = :Nome', {
			replacements: { Nome },
			type: QueryTypes.SELECT
		});

		if (postoColeta.length > 0) {
			return res.json({
				status: 400,
				message: 'Posto de coleta já cadastrado.',
			});
		}

		const createdCollectionPoint = await CollectionPoint.create(
			{
				Nome, 
				Endereco: endereco, 
				Numero: numero, 
				CEP: cep, 
				Bairro: bairro, 
				Cidade: cidade, 
				Estado: estado, 
				Telefone: telefone, 
				Complemento: complemento, 
				Ativo: 1,
				IdCentroReparo
			}
		);

		const idPostoColeta = createdCollectionPoint.dataValues.idPostoColeta

		if (createdCollectionPoint) {
			const createdClientContract = await ClienteContratual.create(
				{
					NomeFantasia: Nome, 
					Rua: endereco, 
					Numero: numero, 
					CEP: cep, 
					Bairro: bairro, 
					Cidade: cidade, 
					Estado: estado, 
					DtCadastro: new Date(),
					Telefone1: telefone, 
					Complemento: complemento,
					Ativo: 1,
					IdUsuarioGestaoCadastro: idUsuarioCadastro,
					idPostoColeta: idPostoColeta
				}
			);

			if (createdClientContract) {
				return res.json({
					status: 201,
					message: 'Posto de Coleta e Cliente Contratual cadastrado com sucesso.',
					idPostoColeta,
					idClientContract: createdClientContract.dataValues.IdClienteContratual
				});
			} else {
				return res.json({
					status: 400,
					message: 'Erro ao cadastrar cliente contratual.'
				});
			}
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao cadastrar posto de coleta e usuário.',
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.',
		});
	}
};
