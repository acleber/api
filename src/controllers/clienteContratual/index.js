const { QueryTypes } = require("sequelize");
const ClienteContratual = require("../../models/ClienteContratual");
const Contratos = require("../../models/Contratos");

exports.clienteContratualIndex = async (req, res) => {
	try {
		const [results, metadata] = await ClienteContratual.sequelize.query('SELECT * FROM ClienteContratual');

		if (!results) {
			return res.json({
				status: 200,
				message: 'Não existe cliente contratual cadastrado no banco de dados.'
			});
		}

		return res.json({
			status: 200,
			message: 'Cliente contratual selecionados(as) com sucesso.',
			results,
			metadata
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		})
	}
}

exports.clienteContratualShowArchive = async (req, res) => {
	try {
		const idCollectionPoint = req.params.idCollectionPoint;

		const results = await ClienteContratual.sequelize.query(
			`SELECT ClienteContratual.*, ArquivosContratos.CaminhoArquivo
				FROM ClienteContratual
				INNER JOIN Contratos ON
						Contratos.IdClienteContratual = ClienteContratual.IdClienteContratual
				INNER JOIN ArquivosContratos ON
						ArquivosContratos.IdContrato = Contratos.IdContrato
				where ClienteContratual.idPostoColeta = :idCollectionPoint and ClienteContratual.Ativo = 1`,
			{
				replacements: { idCollectionPoint },
				type: QueryTypes.SELECT
			}
		);

		if (!results) {
			return res.json({
				status: 200,
				message: 'Não existe cliente contratual cadastrado no banco de dados.'
			});
		}

		return res.json({
			status: 200,
			message: 'Cliente contratual selecionados(as) com sucesso.',
			results
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		})
	}
}

exports.clienteContratualShow = async (req, res) => {
	try {
		const idCollectionPoint = req.params.idCollectionPoint;

		const [results] = await ClienteContratual.sequelize.query(
			`SELECT *
			FROM ClienteContratual
			WHERE ClienteContratual.idPostoColeta = :idCollectionPoint and ClienteContratual.Ativo = 1`,
			{
				replacements: { idCollectionPoint },
				type: QueryTypes.SELECT
			}
		);

		if (!results) {
			return res.json({
				status: 200,
				message: 'Não existe cliente contratual cadastrado no banco de dados.'
			});
		}

		return res.json({
			status: 200,
			message: 'Cliente contratual selecionados(as) com sucesso.',
			results
		});
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		})
	}
}

exports.clienteContratualUpdate = async (req, res) => {
	try {
		const {
			NomeFantasia,
			RazaoSocial,
			Rua,
			Bairro,
			CEP,
			Numero,
			Estado,
			CNPJ,
			InscricaoEstatual,
			InscricaoMunicipal,
			Email,
			Telefone1,
			Telefone2,
			NomeContato,
			idUsuarioCadastro,
			Cidade,
			complemento,
			numeroContrato,
			idCollectionPoint
		} = req.body;

		const idClientContract = req.params.idClientContract;

		const updatedClientContract = await ClienteContratual.update(
			{
				RazaoSocial,
				NomeFantasia,
				CNPJ,
				Rua,
				Numero,
				CEP,
				Bairro,
				Cidade,
				Estado,
				Telefone1,
				Telefone2,
				IdUsuarioGestaoCadastro: idUsuarioCadastro,
				InscricaoEstatual,
				InscricaoMunicipal,
				Email,
				NomeContato,
				Complemento: complemento
			}, {
			where: {
				IdClienteContratual: idClientContract
			}
		});

		if (updatedClientContract) {
			const [results] = await Contratos.sequelize.query('SELECT * FROM Contratos WHERE NumeroContrato = :numeroContrato OR idPostoColeta = :idCollectionPoint', {
				replacements: { numeroContrato, idCollectionPoint },
				type: QueryTypes.SELECT
			});

			if (!results) {
				const contractCreated = await Contratos.create({
					NumeroContrato: numeroContrato,
					IdClienteContratual: idClientContract,
					Ativo: 1,
					idPostoColeta: idCollectionPoint
				});

				if (contractCreated) {
					return res.json({
						status: 200,
						message: 'Cliente contratual e contrato cadastrado com sucesso.',
						idContract: contractCreated.dataValues.IdContrato
					});
				} else {
					return res.json({
						status: 400,
						message: 'Erro ao cadastrar cliente contratual e contrato.',
					});
				}
			} else {
				return res.json({
					status: 400,
					message: 'Contrato já cadastrado e/ou posto de coleta já possui um contrato.',
				});
			}
		} else {
			return res.json({
				status: 400,
				message: 'Erro ao alterar cliente contratual.'
			});
		}
	} catch (error) {
		return res.json({
			status: 500,
			message: 'Erro ao processar a solicitação.'
		});
	}
}
