const User = require("../../models/UsuariosGestao");
const { generateHashPassword } = require("../../functions");

exports.userIndex = async(req, res) => {
    try {
        const users = await User.findAll({
            attributes: ['id', 'name', 'email', 'permission', 'idCentroReparo', 'createdAt', 'updatedAt'],
            where: {
                status: 1
            }
        });

        if (users) {
            return res.json({
                status: 200,
                message: 'Usuários selecionados com sucesso.',
                users
            });
        } else {
            return res.json({
                status: 200,
                message: 'Não existe usuário cadastrado em nosso banco de dados.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.userShow = async(req, res) => {
    try {
        const id = req.params.id;

        const user = await User.findOne({
            attributes: ['id', 'name', 'email', 'permission', 'idCentroReparo', 'createdAt', 'updatedAt'],
            where: {
                id
            }
        });

        if (user) {
            return res.json({
                status: 200,
                message: 'Usuário(a) selecionado(a) com sucesso.',
                user
            });
        } else {
            return res.json({
                status: 200,
                message: 'Não existe usuário(a) cadastrado(a) em nosso banco de dados.',
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.userStore = async(req, res) => {
    try {
        const { name, email, password, permission, idCentroReparo = null } = req.body;

        const [user, created] = await User.findOrCreate({
            where: {
                email
            },
            defaults: {
                name,
                email,
                password: generateHashPassword(password),
                permission: permission,
                idCentroReparo
            }
        });

        if (created) {
            return res.json({
                status: 200,
                message: 'Usuário(a) cadastrado(a) com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao cadastrar usuário(a).'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }

}

exports.userUpdate = async(req, res) => {
    try {
        const id = req.params.id;
        const { name, email, permission, idCentroReparo } = req.body;

        const [updated] = await User.update({
            name,
            email,
            permission,
            idCentroReparo
        }, {
            where: {
                id
            }
        });

        if (updated === 1) {
            return res.json({
                status: 200,
                message: 'Usuário(a) alterado(a) com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao alterar usuário(a).'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}

exports.userDelete = async(req, res) => {
    try {
        const id = req.params.id;

        const [updated] = await User.update({
            status: 0
        }, {
            where: {
                id
            }
        });

        if (updated === 1) {
            return res.json({
                status: 200,
                message: 'Usuário(a) deletado(a) com sucesso.'
            });
        } else {
            return res.json({
                status: 400,
                message: 'Erro ao deletar usuário(a).'
            });
        }
    } catch (error) {
        return res.json({
            status: 500,
            message: 'Erro ao processar a solicitação.',
        });
    }
}
