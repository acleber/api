# Sistema de Gestão Pegasus. v1.0

Este projeto tem a finalidade de apresentar o backend do sistema de gestão para Postos de Coleta.
Esta camada do projeto foi desenvolvido em JavScript, com as bibliotecas mais usados no mercado como express, ejs, jsonwebtoken, sequelize entre outras.

## Instalação

Para instalação dos pacotes necessários para o bom funcionamento da api

```js
  yarn install
```

## Funcionalidades

As funcionalidade descritas aqui serão apresentadas em ordem por endpoints.

### `Auth`

Controller que serve para fazer toda a parte de autenticação do usuário e envio de email para alteração da senha.

#### `authenticated`

Funcionalidade para verificar se o usuário tem permissão de uso do sistema.

Entrada de dados:
```js
  {
    email: string,
    password: string
  }
```

Retorno de dados:
```js
  {
    status: number,
    message: string,
    user: objeto,
    token: string
  }
```

#### `forgotPassword`

Funcionalidade para envio de email com o link para alterar senha.

Entrada de dados:
```js
  {
    email: string,
  }
```

Retorno de dados:
```js
  {
    status: number,
    message: string
  }
```

#### `resetPassword`

Funcionalidade para alteração da senha.

Entrada de dados:
```js
  {
    email: string,
    token: string
  }
```

Retorno de dados:
```js
  {
    status: number,
    message: string
  }
```

#### `verificationToken`

Funcionalidade para verificar se o token fornecido ao usuário está valido.

Entrada de dados:
```js
  token - através de parametros na url
```

Retorno de dados:
```js
  {
    status: number,
    message: string
  }
```

### `User`

Controller que serve para fazer toda a parte de cadastro, leitura, alteração e deletar o cadastro de usuário gerado pelo nosso sistema.

#### `userIndex`

Funcionalidade para buscar todos os usuários cadastrados.

Retorno de dados:
```js
  {
    status: number,
    message: string,
    users: [{
        id: numbere,
        name: string,
        email: string,
        permission: number,
        idCentroReparo: number | null,
        createdAt: string,
        updatedAt: string
    }]
  }
```

`Observações importantes:` Este documento poderá ser alterado futuramente de acordo com as atualizações do sistema.
